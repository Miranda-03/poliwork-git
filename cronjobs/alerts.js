const CronJob = require('cron').CronJob;
const Nota = require('../src/models/NotaModel');
const { connectToDB } = require('../src/utils/DbUtils')
const Alumno = require('../src/models/AlumnoModel')
const { agregarNotificacion } = require('../src/CRUDs/AlumnoCRUD/Controller')
const { codigo200, error500, error404, error400 } = require('../src/utils/utils');
const {enviarNotificacionWS} = require('../src/utils/WSClientUtils')
const mongoose = require("mongoose")

connectToDB();

let agregarNotificaciones = (nota, tipo, fecha) => {
    const ObjectId = mongoose.Types.ObjectId;
    let promiseArr = nota.Alumnos.map((alumno) => {
        if (alumno.estado === "aceptada") {
            let notificacion = {
                "_id": new ObjectId,
                "DescripcionNotificacion": "una notificacion",
                "Tipo": tipo,
                "TituloEvento": nota.Titulo,
                "fecha": fecha
            }
            agregarNotificacion(notificacion, alumno.Alumno, alumno.Usuario.mail)
        }
    })

    Promise.all(promiseArr).then(function (data) {
        // console.log(data)
    }).catch(function (error) {
        console.log(error)
    })
}


const generalJob = new CronJob('*/1 * * * *', () => {
    let start = Date.now();
    Nota.find()
        .then((notas) => {
            notas.map((nota) => {
                nota.Recordatorios.map((recordatorios) => {
                    recordatorios.FechasRecordar.map((fechasrecordar) => {
                        fechasrecordar.Fechas.map((fecha) => {
                            if (start > fecha.date) agregarNotificaciones(nota, "Nota, recordatorio", fecha)
                        })
                    })
                })

                nota.Reuniones.map((reunion) => {
                    if (start > reunion.Dia.date) agregarNotificaciones(nota, "Nota, reunion", reunion.Dia)
                })

                if (start > nota.FechaFinal && nota.Estado != "vencida") {
                    Nota.cambiarEstado(nota._id, "vencida")
                        .then(() => {
                            console.log("estado cambiado")
                            agregarNotificaciones(nota, "Nota vencida", null)
                        })
                        .catch((error) => {
                            console.log(error)
                        })
                }
            })
        })
        .catch((err) => {
            console.log(err)
        })
})


generalJob.start();