var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session')
var logger = require('morgan');

const cors = require("cors");

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var alumnosRouter = require('./src/CRUDs/AlumnoCRUD/Router.js')
var materiaRouter = require('./src/CRUDs/MateriaCRUD/Router')
var notaRouter = require('./src/CRUDs/NotaCRUD/Router')
var authRouter = require('./src/auth/Router')
var solicitudRouter = require('./src/CRUDs/SolicitudCRUD/Router')
var ChatRouter = require('./src/CRUDs/MessageCRUD/Router')

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('trust proxy', 1)

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const whiteList = ['http://localhost:3000', 'https://poliwork.onrender.com']
app.use(cors({
  credentials: true, origin: function (origin, callback) {
    //let url = process.env.NODE_ENV !== 'production' ? "http://localhost:3000" : "https://poliwork.onrender.com"
    if ( whiteList.includes(origin) || origin == undefined) {
      callback(null, true)
    }
    else {
      callback(new Error("Not allowed by CORS"))
    }
  }
}))



app.use('/', indexRouter);
app.use('/auth', authRouter);
app.use('/alumnos', alumnosRouter);
app.use('/materias', materiaRouter);
app.use('/notas', notaRouter);
app.use('/users', usersRouter);
app.use('/solicitud', solicitudRouter);
app.use('/chat', ChatRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
