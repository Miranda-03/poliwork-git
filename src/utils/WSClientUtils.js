const { io } = require("socket.io-client")
const { codigo200 } = require('./utils')
let url = process.env.NODE_ENV !== 'production' ? "http://localhost:3001" : `https://poliwork-api.onrender.com`


var socket = io(url);

socket.on('connect', () => {
    console.log('connected');
});

module.exports.enviarNotificacionWS = (data) => {
    const message = data.mensaje
    console.log(message)
    const room = data.room
    socket.emit("send_message", { message, room });
}