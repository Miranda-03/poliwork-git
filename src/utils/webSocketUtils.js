const { Server } = require("socket.io");
const {saveMessage} = require("../CRUDs/MessageCRUD/Controller")
//const {msgToRedis, connectRedis} = require("./redisUtil")

module.exports.conectarWS = (server) => {
    io = new Server(server, {
        cors: {
            origin: '*',
        },
    });

    io.on("connection", (socket) => {


        socket.on("join_room", (data) => {
            socket.join(data);
        });

        socket.on("send_message", (data) => {
            socket.to(data.room).emit("receive_message", data);
        });

        socket.on("send_msg", (data) => {
            saveMessage(data)
            socket.to(data.userReceiving).emit("get_msg", data)
        })
    });

}

