const fs = require('fs');
const fsPromises = fs.promises;
const config = require("config")


module.exports.codigo200 = (res,mensaje) => {
    return res.status(200).json({
        code: 10,
        message: mensaje
    })
}

module.exports.error404 = (res,mensajeError) => {
    return res.status(404).json({
        code: 20,
        message: mensajeError
    })
}

module.exports.error401 = (res,mensajeError) => {
    return res.status(401).json({
        code: 21,
        message: mensajeError
    })
}

module.exports.error400 = (res,mensajeError) => {
    return res.status(400).json({
        code: 22,
        message: mensajeError
    })
}

module.exports.error500 = (res = "error",mensajeError) => {
    return res.status(500).json({
        code: 23,
        message: mensajeError
    })
}