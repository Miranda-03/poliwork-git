const redis = require("redis")
const client = redis.createClient();
const subscriber = client.duplicate();
//subscriber.on('error', err => console.error(err));
//subscriber.connect();

const maxMessageCached = 10

module.exports.connectRedis = () => {
    client.on('error', err => console.log('Redis Client Error', err));
    client.connect({ host: "0.0.0.0", port: 6379 });
}

module.exports.msgToRedis = (data) => {
    const message = data
    const key = data.key

    client.rPush(key, JSON.stringify(message))
    .then(() => {
        deleteLastMSG(key)
    })
}

module.exports.getMessages = (key) => {
    console.log(key)
    return client.lRange(key, 0 ,-1)
}

const deleteLastMSG = (key) => {
    client.lLen(key)
    .then((number) => {
        if (number > maxMessageCached){
            client.lPop(key)
        }
    })
}

const listener = (message, channel) => console.log(message, channel);

module.exports.SuscribeToChannel = (channel) => {
    console.log(channel);
    subscriber.subscribe(channel, listener);
}