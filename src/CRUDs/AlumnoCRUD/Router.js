const express = require('express');
const RouterAlumno = express.Router();
const { verAlumno, agregarAlumno, filtrarPorId, modificarAlumno, eliminarAlumno, getAmigos, quitarAmigo, quitarSolicitud } = require("./Controller")
const {jwtValidator} = require('../../auth/Validator')
const { readToken} = require('../../utils/tokenUtils')

//localhost:3000/alumnos?Nombre=Martín
RouterAlumno.route("/")
    .get(jwtValidator, readToken, verAlumno)
    .post(agregarAlumno)

RouterAlumno.route("/:id") 
    .get( jwtValidator, readToken, filtrarPorId)
    .patch( jwtValidator, readToken, modificarAlumno)

RouterAlumno.route('/:name')
    .delete(jwtValidator, readToken, eliminarAlumno)

RouterAlumno.route("/:id/noti")
    .delete( jwtValidator, readToken, quitarSolicitud)

RouterAlumno.route("/:name/amigos")
    .get(jwtValidator, readToken, getAmigos)
    .delete(jwtValidator, readToken, quitarAmigo)

module.exports = RouterAlumno