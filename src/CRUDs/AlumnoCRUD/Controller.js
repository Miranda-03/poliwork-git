const { codigo200, error500, error404, error400 } = require('../../utils/utils');
const Alumno = require("../../models/AlumnoModel");
const { Router } = require('express');
const { body, validationResult } = require('express-validator');
const { enviarNotificacionWS } = require('../../utils/WSClientUtils')
const mongoose = require("mongoose")



module.exports.agregarAlumno = (req, res) => {

    const { Nombre, Apellido, Curso, Usuario } = req.body;

    let alumno = ""
    alumno = new Alumno({
        Nombre,
        Apellido,
        Curso,
        Usuario
    })

    alumno.save()
        .then(() => {
            codigo200(res, "el alumno se agrego")
        })
        .catch((error) => {
            error500(res, error)
        })

}

module.exports.verAlumno = (req, res) => {
    const filtro = req.query.User
    const filtroAplicar = {
        ...(filtro && ({ 'Usuario.name': filtro }))
    }

    Alumno.find(filtroAplicar)
        .then(data => {
            if (data != undefined) codigo200(res, data)
            else error404(res, "No hay alumnos con ese nombre")// array vacio
        })
        .catch(error => error500(error))
}

module.exports.filtrarPorId = (req, res) => {
    const buscar = Alumno.buscarPorId(req.params.id);

    buscar
        .then(resultado => {
            if (resultado != undefined) return codigo200(res, resultado);
            error404(res, "No hay alumnos con esa id")
        })
        .catch(error => {
            error500(res, error)
        })
}

module.exports.eliminarAlumno = (req, res) => {
    const eliminar = Alumno.deleteOne({ "Usuario.name": req.params.name })

    eliminar
        .then(resultado => {
            if (resultado.deletedCount == 0) error404(res, "No hay alumnos con esa id para borrar")
            else codigo200(res, resultado.deletedCount)
        })
        .catch(error => {
            error500(res, error)
        })
}

module.exports.modificarAlumno = (req, res) => {
    Alumno.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true })
        .then(data => {
            if (data == undefined) error404(res, "No hay alumnos con esa id para modificar")
            else codigo200(res, data)
        })
        .catch(error => error500(res, error))
}

module.exports.getAmigos = (req, res) => {
    Alumno.findOne({ 'Usuario.name': req.params.name }, { 'Amigos': 1, "_id": 0 })
        .then((data) => {
            codigo200(res, data)
        })
        .catch((error) => {
            error500(res, error)
        })
}

module.exports.agregarNotificacion = async (notificacion, idAlumno, mail) => {
    if (notificacion.fecha._id == undefined) {
        return Alumno.findOneAndUpdate({ 'Usuario.name': idAlumno }, { $push: { Notificaciones: notificacion } }, { new: true })
            .then((response) => {
                if (response == undefined) throw new Error("No hay alumnos con esa id para agregegar solicitud");
                let data = {
                    "mensaje": {
                        contenido: notificacion.DescripcionNotificacion,
                        tipo: notificacion.Tipo
                    },
                    "room": response.Usuario.name
                }
                enviarNotificacionWS(data)
            })
            .catch(error => {
                return Promise.reject(error);
            })
    }
    else {
        Alumno.findOne({ "Notificaciones.fecha._id": notificacion.fecha._id })
            .then((data) => {
                if (data == undefined) {
                    return Alumno.findOneAndUpdate({ 'Usuario.name': idAlumno }, { $push: { Notificaciones: notificacion } }, { new: true })
                        .then((alumnodata) => {
                            if (alumnodata == undefined) throw new Error("No hay alumnos con esa id para agregegar solicitud");
                            let dataN = {
                                "mensaje": {
                                    contenido: notificacion.DescripcionNotificacion,
                                    tipo: notificacion.Tipo
                                },
                                "room": alumnodata.Usuario.name
                            }
                            enviarNotificacionWS(dataN)
                        })
                        .catch(error => {
                            return Promise.reject(error);
                        })
                }
            })
            .catch((error) => {
                return Promise.reject(error);
            })

    }

}


module.exports.quitarAmigo = (req, res) => {
    Alumno.findOneAndUpdate({ 'Usuario.name': req.params.name }, { $pull: { Amigos: { $eq: req.query.nameAmigo } } })
        .then(() => {
            Alumno.findOneAndUpdate({ 'Usuario.name': req.query.nameAmigo }, { $pull: { Amigos: { $eq: req.params.name } } })
                .then((response) => {
                    const date = new Date();
                    const ObjectId = mongoose.Types.ObjectId;
                    let notificacion = {
                        "_id" : new ObjectId,
                        "DescripcionNotificacion": `${req.params.name} te eliminó de la lista de amigos :(`,
                        "Tipo": undefined,
                        "_idEvento": undefined,
                        "fecha": date
                    }
                    console.log("llega aca")
                    this.agregarNotificacion(notificacion, req.query.nameAmigo, undefined)
                    codigo200(res, 'amigo eliminado')
                })
                .catch((error) => {
                    error500(res, error)
                })
        })
        .catch((error) => {
            error500(res, error)
        })
}

module.exports.agregarSolicitud = (solicitud) => {
    return new Promise((resolve, reject) => {
        console.log(`EL _ID ES: ${solicitud}`)
        Alumno.findOneAndUpdate({ 'Usuario.name': solicitud.NameDestinatario }, { $push: { Solicitudes: solicitud } }, { new: true })
            .then(alumno => {
                if (alumno == undefined) reject('No hay alumnos con ese nombre de usuario para agregegar solicitud')
                let data = {
                    "mensaje": {
                        remitente: solicitud.NameRemitente,
                        tipo: solicitud.Tipo
                    },
                    "room": alumno.Usuario.name
                }
                enviarNotificacionWS(data)
                resolve("solicitud agregada")
            })
            .catch(error => { reject(error) })
    });

}


module.exports.quitarSolicitud = (req, res) => {
    const idNot = req.params.id
    console.log( `el nombre es: ${req.query.nameUsuario}`)
    Alumno.findOneAndUpdate({'Usuario.name' : req.query.nameUsuario} , {$pull : {Notificaciones : {_id : idNot}}})
    .then((data) => {
        if (data == undefined) error404(res, "No hay alumnos con ese nombre de usuario para borrar la notificación")
        codigo200(res, data)
    })
    .catch((error) => {
        error500(res, error)
    })
}

module.exports.agregarNotaAlumno = (nota, idAlumno) => {
    Alumno.findOneAndUpdate({ _id: idAlumno }, { $push: { Notas: nota } }, { new: true })
        .then(data => {
            if (data == undefined) error404(res, "No hay alumnos con esa id para modificar")
            codigo200(res, nota)
        })
        .catch(error => error500(res, "esta en la otra función"))
}
