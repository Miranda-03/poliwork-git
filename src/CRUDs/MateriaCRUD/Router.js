const express = require('express');
const RouterMateria = express.Router();
const {verMaterias, modificarMateria, filtrarPorId, agregarMateria, eliminarMateria} = require("./Controller")

//localhost:3000/materias?Nombre=Matemática
//localhost:3000/materias/
RouterMateria.route("/")
    .get(verMaterias)
    .post(agregarMateria)

RouterMateria.route("/:id")
    .get(filtrarPorId)
    .patch(modificarMateria)
    .delete(eliminarMateria)

module.exports = RouterMateria