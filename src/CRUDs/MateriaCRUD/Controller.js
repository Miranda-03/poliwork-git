const Materia = require("../../models/MateriaModel");
const { codigo200, error500, error404 } = require('../../utils/utils');

module.exports.agregarMateria = (req, res) => {

    const { Nombre, Profesor, Dias } = req.body

    let materia = ""

    materia = new Materia({
        Nombre,
        Profesor,
        Dias
    })

    materia.save()
        .then(() => {
            codigo200(res, "la materia se agrego con éxito")
        })
        .catch((error) => {
            error500(res, error)
        })

}

module.exports.verMaterias = (req, res) => {

    const filtro = req.query.nombre
    const filtroAplicar = {
        ... (filtro && ({ Nombre: filtro }))
    }

    Materia.find(filtroAplicar)
        .then(data => {
            if (data != undefined) codigo200(res, data)
            else error404(res, "No hay materias con ese nombre")// array vacio
        })
        .catch(error => error500(error))
}

module.exports.filtrarPorId = (req, res) => {
    const buscar = Materia.buscarPorId(req.params.id);

    buscar
        .then(resultado => {
            if (resultado != undefined) return codigo200(res, resultado);
            error404(res, "No hay materias con esa id")
        })
        .catch(error => {
            error500(res, error)
        })
}

module.exports.eliminarMateria = (req, res) => {

    const eliminar = Materia.deleteById(req.params.id)

    eliminar
        .then(resultado => {
            if (resultado.deletedCount == 0) error404(res, "la materia no existe para eliminar")
            else codigo200(res, resultado.deletedCount)
        })
        .catch(error => {
            error500(res, error)
        })

}

module.exports.modificarMateria = (req, res) => {
    Materia.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true })
        .then(data => {
            if (data == undefined) error404(res, "la materia no existe para modificar")
            else codigo200(res, data)
        })
        .catch(error => error500(res, error))
}