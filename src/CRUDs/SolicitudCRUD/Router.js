const express = require('express');
const RouterSolicitud = express.Router();
const { handleSolicitud, crearSolicitudAmistad } = require("./Controller")


//localhost:3000/solicitud/
RouterSolicitud.route("/")
    .put(handleSolicitud)
    .post(crearSolicitudAmistad)

module.exports = RouterSolicitud