const { codigo200, error500, error404, error400 } = require('../../utils/utils');
const Alumno = require("../../models/AlumnoModel");
const { Router } = require('express');
const { body, validationResult } = require('express-validator');
const Nota = require('../../models/NotaModel');
const Solicitud = require('../../models/SolicitudesModel')
const { agregarSolicitud } = require('../AlumnoCRUD/Controller')
const { enviarNotificacionWS } = require('../../utils/WSClientUtils')
const { agregarNotificacion } = require('../AlumnoCRUD/Controller')
const mongoose = require("mongoose")


module.exports.obtenerSolicitudesAlumno = (req, res) => {
    Solicitud.find({ _id: req.params.id })
        .then((response) => {
            codigo200(res, response)
        })
        .catch(error => {
            error500(res, error)
        })
}

//db.alumnos.findOne({'Usuario.name' : 'agus-2004' , Amigos : 'sdnxdxd' })
const verificarUsuarioSolicitud = (solicitud) => {
    return new Promise((resolve, reject) => {
        //{'Usuario.name' : 'agus-2004' , Amigos : {$elemMatch : {$eq : 'sdnxdxd'}} }

        Alumno.findOne({ 'Usuario.name': solicitud.NameRemitente, Amigos: { $elemMatch: { $eq: solicitud.NameDestinatario } } })
            .then((response) => {
                if (response != undefined) reject("El usuario ya esta en tu lista de amigos.")
                else {
                    Alumno.findOne({ 'Usuario.name': solicitud.NameRemitente, Solicitudes: { $elemMatch: { NameDestinatario: solicitud.NameDestinatario } } })
                        .then((response) => {
                            if (response != undefined) reject("Ya le enviaste una solicitud a ese usuario.")
                            resolve()
                        })
                }
            })
            .catch(error => { reject(error) })
    })
}

module.exports.crearSolicitudAmistad = (req, res) => {

    const { AlumnoCreador, AlumnoDestinatario } = req.body

    let solicitudAmistad = ""
    solicitudAmistad = new Solicitud({
        "NameRemitente": AlumnoCreador,
        "Estado": "pendiente",
        "Tipo": "Amistad",
        "NameDestinatario": AlumnoDestinatario,
        "idNota": undefined
    })

    verificarUsuarioSolicitud(solicitudAmistad)
        .then(() => {
            agregarSolicitud(solicitudAmistad)
                .then((response) => {
                    Alumno.findOne({ 'Usuario.name': AlumnoCreador })
                        .then((alumno) => {
                            solicitudAmistad.save()
                            codigo200(res, alumno)
                        })
                        .catch(error => {
                            error500(res, error)
                        })
                })
                .catch(error => {
                    error500(res, error)
                })
        })
        .catch(error => { error500(res, error) });
}

module.exports.handleSolicitud = (req, res) => {
    const Solicitud = req.body.Solicitud
    const respuesta = req.body.respuesta
    const alumnoName = req.body.name
    const date = new Date();
    const ObjectId = mongoose.Types.ObjectId;
    let notificacion = {
        "_id" : new ObjectId,
        "DescripcionNotificacion": `Solicitud de ${Solicitud.Tipo} para ${alumnoName} ${respuesta}`,
        "Tipo": Solicitud.Tipo,
        "_idEvento": 0,
        "fecha": date
    }
    agregarNotificacion(notificacion, Solicitud.NameRemitente, Solicitud.NameRemitente)
        .then((data) => {
            console.log(data)
        })
        .catch((error) => {
            console.log(error)
        })
    //db.alumnos.findOneAndUpdate({ _id:2 }, { $pull :  {Solicitudes:{_id: 79} } } )
    Alumno.findOneAndUpdate({ 'Usuario.name': alumnoName }, { $pull: { Solicitudes: Solicitud } })
        .then((alumno) => {
            //const solicitudID = alumno.Solicitudes.find(solicitud => solicitud == _idSolicitud)
            //console.log(alumno)
            if (Solicitud.Tipo === "Nota") {
                if (respuesta === "aceptada") {
                    Nota.findOneAndUpdate({ _id: Solicitud.idNota, 'Alumnos.Alumno': Solicitud.NameDestinatario }, { $set: { 'Alumnos.$.estado': "aceptada" } })
                        .then((nota) => {
                            console.log(nota)
                            Alumno.findOneAndUpdate({ 'Usuario.name': alumnoName }, { $push: { Notas: nota } }, { new: true })
                                .then(data => {
                                    if (data == undefined) error404(res, "No hay alumnos con esa id para agregar una nota");
                                    codigo200(res, nota);
                                })
                                .catch((error) => {
                                    error500(res, error)
                                })
                        })
                        .catch(error => {
                            error500(res, error);
                        })

                }
                else if (respuesta === "rechazada") {
                    Nota.findOneAndUpdate({ _id: Solicitud.idNota, 'Alumnos.Alumno': Solicitud.NameDestinatario }, { $set: { 'Alumnos.$.estado': "rechazada" } })
                        .then((nota) => {
                            codigo200(res, nota)
                        })
                        .catch(error => {
                            error500(res, error)
                        })
                }

            }
            else if (Solicitud.Tipo === "Amistad") {

                if (alumnoName == Solicitud.NameRemitente) error400(res, "no se puede responder a una solicitud propia")

                if (respuesta === "aceptada") {
                    Alumno.findOne({ 'Usuario.name': Solicitud.NameRemitente }, { 'Usuario': 1, _id: 0 })
                        .then((alumno) => {
                            Alumno.findOneAndUpdate({ 'Usuario.name': Solicitud.NameDestinatario }, { $push: { Amigos: alumno.Usuario.name }, $pull: { Solicitudes: Solicitud } })
                                .then((update) => {
                                    if (update == undefined) error404(res, "No hay alumnos con esa id para agregar un amigo")
                                    Alumno.findOneAndUpdate({ 'Usuario.name': Solicitud.NameRemitente }, { $push: { Amigos: update.Usuario.name }, $pull: { Solicitudes: Solicitud } })
                                        .then((update) => {
                                            if (update == undefined) error404(res, "No hay alumnos con esa id para agregar un amigo")
                                            codigo200(res, update)
                                        })
                                        .catch((error) => {
                                            error500(res, error)
                                        })
                                })
                                .catch((error) => {
                                    error500(res, error)
                                })
                        })
                        .catch((error) => {
                            error500(res, error)
                        })
                }
                else {
                    Alumno.findOneAndUpdate({ 'Usuario.name': Solicitud.NameDestinatario }, { $pull: { Solicitudes: Solicitud } })
                        .then((update) => {
                            if (update == undefined) error404(res, "No existe el usuario")
                            Alumno.findOneAndUpdate({ 'Usuario.name': Solicitud.NameRemitente }, { $pull: { Solicitudes: Solicitud } })
                                .then((update) => {
                                    if (update == undefined) error404(res, "No existe el usuario")
                                    codigo200(res, update)
                                })
                                .catch((error) => {
                                    error500(res, error)
                                })
                        })
                        .catch((error) => {
                            error500(res, error)
                        })
                }
            }


        })
        .catch(error => error500(res, error))

}