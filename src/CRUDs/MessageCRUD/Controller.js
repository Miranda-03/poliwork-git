const { codigo200, error500, error404, error400 } = require('../../utils/utils');
const msg = require("../../models/MessageModel");
const { Router } = require('express');
const { body, validationResult } = require('express-validator');
const { enviarNotificacionWS } = require('../../utils/WSClientUtils')
const mongoose = require("mongoose")
const { msgToRedis, connectRedis, getMessages } = require("../../utils/redisUtil")



module.exports.saveMessage = (data) => {

    const key = data.key
    console.log(data.messageBody)
    const messageBody = data.messageBody
    const userSending = data.userSending
    const userReceiving = data.userReceiving

    let sendMsg = ""
    sendMsg = new msg({
        key,
        userSending,
        userReceiving,
        messageBody,
    })

    sendMsg.save()
        .then((data) => {
            console.log("se guardo")
            msgToRedis(data)
        })
        .catch((error) => {
            console.log(error)
        })

}

const setJSON = (data, cache = false) => {
    let msgobj = []

    data.map((value) => {
        if (cache) {
            msgToRedis(value)
            console.log("entra")
        }
        else {
            let obj = JSON.parse(value)
            msgobj.push(obj)
        }
    })
    return msgobj
}

module.exports.returnMessages = (req, res) => {

    const key = req.params.key
    const cacheResults = getMessages(key)
    let results;
    cacheResults
        .then((data) => {
            if (data == "") {
                msg.find({ 'key': key }).sort('-date').limit(10)
                    .then(data => {
                        if (data != undefined) {
                            setJSON(data, true)
                            codigo200(res, data)
                        }
                        else error404(res, "chat not found")// array vacio
                    })
                    .catch(error => error500(res, error))
            }
            else {
                results = setJSON(data)
                codigo200(res, results)
            }
        })
}

