const express = require('express');
const RouterChat = express.Router();
const {returnMessages} = require("./Controller")

//localhost:3000/materias?Nombre=Matemática
//localhost:3000/materias/
RouterChat.route("/:key")
    .get(returnMessages)

module.exports = RouterChat