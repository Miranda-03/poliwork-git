const Alumno = require('../../models/AlumnoModel')
const { codigo200, error500, error404 } = require('../../utils/utils');
const { body } = require("express-validator")
const { validationResult } = require('express-validator');

module.exports.checksIDs = [

    body('Alumnos').isArray()
        .custom(value => {

            let ids_array = []

            return Alumno.find()
                .then(function (idAlumnos) {

                    for (let i = 0; i < idAlumnos.length; i++) {
                        ids_array.push(idAlumnos[i].Usuario.name)
                    }

                    value.map((idValue) => {
                        console.log(idValue)
                        if (!ids_array.includes(idValue.Alumno)) {
                            throw new Error('no hay alumnos con ese nombre de usuario');
                        }
                    })

                    return Promise.resolve('la id es correcta');

                })
                .catch(function (error) {
                    return Promise.reject(error);
                })

        }),
    (req, res, next) => {

        const errors = validationResult(req)

        if (errors.isEmpty())
            return next()
        error404(res, errors.array());

    }
]