const express = require('express');
const RouterNota = express.Router();
const { verNotas, agregarNota, filtrarPorId, modificarNota, eliminarNota, agregarSolicitudNota } = require("./Controller")
const { checksIDs } = require('./Validator')

//localhost:3000/notas?Titulo=hacerTarea
//localhost:3000/notas/
RouterNota.route("/")
    .get(verNotas)
    .post(checksIDs, agregarNota, agregarSolicitudNota)

RouterNota.route("/:id")
    .get(filtrarPorId)
    .patch(modificarNota)
    .delete(eliminarNota)

module.exports = RouterNota