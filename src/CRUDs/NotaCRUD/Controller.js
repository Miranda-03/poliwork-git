const Nota = require("../../models/NotaModel")
const Solicitud = require('../../models/SolicitudesModel')
const { agregarSolicitud } = require('../AlumnoCRUD/Controller')
const { codigo200, error500, error404 } = require('../../utils/utils');
var mongoose = require('mongoose');

module.exports.agregarNota = (req, res, next) => {

    const {
        Titulo,
        AlumnoCreador,
        FechaFinal,
        Alumnos,
        Recordatorios,
        Descripcion,
        Reuniones,
        Estado
    } = req.body


    console.log( 
        Reuniones)

    let nota = ""

    Reuniones.map((reunion) => {
        return reunion.Dia._id = mongoose.Types.ObjectId();
    })

    nota = new Nota({
        Titulo,
        Descripcion,
        AlumnoCreador,
        FechaFinal,
        Estado,
        Alumnos,
        Recordatorios,
        Reuniones
    })

    nota.save()
        .then((data) => {
            console.log(`LA NUEVA ES: ${data._id}`)
            req.body._id = data._id 
            next()
        })
        .catch((error) => {
            error500(res, error)
        })
}

module.exports.agregarSolicitudNota = (req, res) => {

    const { _id, AlumnoCreador, Alumnos } = req.body


    let promiseArr = Alumnos.map(function (alumno) {

        console.log(`el alumno es: ${alumno.Alumno}`)

        let solicitud = ""
        console.log(`la is es: ${_id}`)
        // return the promise to array
        solicitud = new Solicitud({
            "NameRemitente": AlumnoCreador,
            "Estado": "pendiente",
            "Tipo": "Nota",
            "NameDestinatario": alumno.Alumno,
            "idNota": _id,
        })

        agregarSolicitud(solicitud)


    });

    Promise.all(promiseArr).then(function (data) {
        codigo200(res, data)
    }).catch(function (error) {
        error500(error)
    })

}

module.exports.verNotas = (req, res) => {
    const filtro = req.query.titulo
    const filtroAplicar = {
        ... (filtro && ({ Titulo: filtro }))
    }

    Nota.find(filtroAplicar)
        .then(data => {
            if (data != undefined) codigo200(res, data)
            else error404(res, "No hay notas con ese nombre")// array vacio
        })
        .catch(error => error500(error))
}

module.exports.filtrarPorId = (req, res) => {
    const buscar = Nota.buscarPorId(req.params.id)

    buscar
        .then(resultado => {
            if (resultado != undefined) return codigo200(res, resultado);
            error404(res, "No hay notas con esa id")
        })
        .catch(error => {
            error500(res, error)
        })
}

module.exports.modificarNota = (req, res) => {
    Nota.findOneAndUpdate({ _id: req.params.id },  req.body.nota,  { upsert: true, setDefaultsOnInsert: true })

        .then(data => {
            if (data == undefined) error404(res, "la nota no existe para modificar")
            else codigo200(res, data)
        })
        .catch(error => error500(res, error))
}

module.exports.eliminarNota = (req, res) => {


    console.log(req.params.id)

    Nota.deleteOne({_id: req.params.id})
        .then(resultado => {
            if (resultado.deletedCount == 0) error404(res, "la nota no existe para eliminar")
            else codigo200(res, resultado.deletedCount)
        })
        .catch(error => {
            error500(res, error)
        })
}