const { cookie} = require('express-validator');
const { verifyvalidation } = require('../utils/validatorUtils');
const  jwt_decode = require("jwt-decode");
const { error403 } = require("../utils/utils");
const verifyToken = require('../utils/tokenUtils');

module.exports.jwtValidator = [
    cookie("token")
    .isJWT()
    .withMessage("Debe ser JWT"),
    verifyvalidation
]

