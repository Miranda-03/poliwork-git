const { createToken } = require("../utils/tokenUtils");
const { codigo200, error500, error401 } = require("../utils/utils");
const Alumno = require('../models/AlumnoModel')
const { enviarNotificacionWS } = require('../../src/utils/WSClientUtils')
const bcrypt = require("bcryptjs")

module.exports.validateLogin = (req, res, next) => {

  //Alumno.buscarPorEmail(req.body.user)
  Alumno.findOne({ 'Usuario.email': req.body.user })
    .then((alumno) => {
      if (alumno == undefined || alumno.Usuario.password != req.body.pass) error401(res, "El usuario o la contraseña son incorrectos")
      else {
        req.currentUserData = {
          usuario: alumno.Usuario
        }
        next()
      }
    })
    .catch(error => {
      error500(res, error)
    })
}

module.exports.createSessionToken = (req, res, next) => {


  createToken(req.currentUserData) //hardcodeado
    .then(token => {
      res.userToken = token
      next()
    })
    .catch(error => {

      error500(res, "ocurrio un error inesperado")
    })
}

module.exports.sendLoginResponse = (req, res) => {
  const token = res.userToken
  let url = process.env.NODE_ENV !== 'production' ? "http://localhost:3000" : "https://poliwork.onrender.com"
  const options = {
     httpOnly: false,
     sameSite: "none",
    secure: true,
    maxAge: 3500 * 1000 
  }

  res.cookie("token", token, { ...options })
  res.cookie("isLogged", true, {...options})
  codigo200(res, token) 
}

module.exports.validateRegister = (req, res, next) => {
  const { username, password } = req.body;
  bcrypt.hash(password, 10).then(async (hash) => {
    await User.create({
      username,
      password: hash,
    })
      .then((user) =>
        res.status(200).json({
          message: "created",
          user,
        })
      )
      .catch((error) =>
        res.status(400).json({
          message: "User not created",
          error: error.message,
        })
      );
  });
};
