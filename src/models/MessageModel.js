const mongoose = require("mongoose");
const { Schema } = mongoose;


const MessageModel = new Schema({
   key: String,
   userSending: String,
   userReceiving: String,
   messageBody: String,            

}, { timestamps: true })


module.exports = mongoose.model("msg", MessageModel)