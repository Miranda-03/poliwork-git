const mongoose = require("mongoose");
const { Schema } = mongoose;
const UsuarioSchema = require('./schemas/UsuarioSchema')
const NotificacionSchema = require('./schemas/NotificacionSchema')
const SolicitudesShema = require('./schemas/SolicitudShema')


const AlumnoModel = new Schema({
    Nombre: String,
    Apellido: String,
    Curso: {
        type: String,
        enum: [
            '1A','1B',
            '2A','2B',
            '3A','3B',
            '4A','4B',
            '5A','5B',
            '6A','6B',
        ]
    },
    Usuario: UsuarioSchema,
    Amigos: [String],
    Notas: [Number], 
    Solicitudes: [SolicitudesShema],
    Notificaciones: [NotificacionSchema],                                            

}, { timestamps: true })

AlumnoModel.statics.buscarPorId = function (id) {
    return this.findOne({_id: id})
}

AlumnoModel.statics.deletePorId = function (id) {
    return this.deleteOne({_id: id})
}

AlumnoModel.statics.buscarPorEmail = function (email) {
    return this.findOne({'Usuario.email' : email})
}

module.exports = mongoose.model("Alumno", AlumnoModel)