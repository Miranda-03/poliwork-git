const { Schema } = require("mongoose")
const mongoose = require("mongoose");

const SolicitudesShema = new Schema({
    NameRemitente: String,
    Estado:{
        type: String,
        enum:[
            'pendiente',
            'aceptada',
            'rechazada'           
        ]
    },
    Tipo:{
        type: String,
        enum:[
            'Nota',
            'Amistad'
        ]
    },
    NameDestinatario: String,
    idNota: mongoose.ObjectId
});

module.exports = SolicitudesShema