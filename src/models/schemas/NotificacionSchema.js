const mongoose = require("mongoose")
const { Schema } = mongoose

const NotificacionSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    DescripcionNotificacion: String,
    Tipo: String,
    TituloEvento: String,
    fecha: {
        date : Date,
        _id: mongoose.ObjectId
    }
},{
    timestamps: true                                                                                                              
})
module.exports = NotificacionSchema