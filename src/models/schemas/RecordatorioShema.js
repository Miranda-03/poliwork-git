const mongoose = require("mongoose")
const { Schema } = mongoose
const Reunion = require("./ReunionShema")
const Tarea = require("./TareaShema")
const MateriaSchema = mongoose.model('Materia').schema

const RecordatorioModel = new Schema({
    _id: Number,
    DescripcionRecordatorio: String,
    Materia: [String],
    FechasRecordar:[{
        Fechas : [{date : Date}],
        Tareas: [Tarea]
    }]
},{
    timestamps: true                                                                                                              
})

module.exports = RecordatorioModel
