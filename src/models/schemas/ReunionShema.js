const mongoose = require("mongoose")
const { Schema } = mongoose

const ReunionShema = new Schema({
    tituloReunion : String,
    DescripcionReunion: String,
    Dia: {
        date: Date,
        _id : {
            type: mongoose.Schema.Types.ObjectId}
    },
    Plataforma: String
})


module.exports = ReunionShema