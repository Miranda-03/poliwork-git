const mongoose = require("mongoose")
const { Schema } = mongoose

const TareaShema = new Schema({
    Estado: String,
    DescripcionTarea: String,
    Nota: Boolean,
},{ _id : false })

module.exports = TareaShema