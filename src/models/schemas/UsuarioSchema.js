const { Schema } = require("mongoose")

const UsuarioSchema = new Schema({
    name : String,
    email: String,
    password: String
});

module.exports = UsuarioSchema;