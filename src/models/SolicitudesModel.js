const { Schema } = require("mongoose")
const mongoose = require("mongoose");

const SolicitudesModel = new Schema({
    NameRemitente: String,
    Estado:{
        type: String,
        enum:[
            'pendiente',
            'aceptada',
            'rechazada'           
        ]
    },
    Tipo:{
        type: String,
        enum:[
            'Nota',
            'Amistad'
        ]
    },
    NameDestinatario: String,
    idNota: mongoose.ObjectId
});

SolicitudesModel.static.deletePorId = function (id) {
    return this.deleteOne({_id: id})
}

module.exports = mongoose.model("Solicitud", SolicitudesModel);