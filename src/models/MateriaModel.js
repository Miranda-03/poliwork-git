const mongoose = require("mongoose")
const { Schema } = mongoose

const MateriaModel = new Schema({
    Nombre: String,
    AlumnoCreador : String,
    Profesor: String,
    Dias: [{
        Dia: String,
        Horarios: [{
            inicio: Date,
            final: Date
        }]
    }],
})

MateriaModel.statics.buscarPorId = function (id) {
    return this.findOne({_id: id})
}
MateriaModel.statics.modificarMateria = function (id) {
    return this.findOneAndUpdate({_id: id})
    }

MateriaModel.statics.deletePorId = function (id) {
    return this.deleteOne({_id: id})
}

MateriaModel.statics.deletePorNombre = function (nombre) {
    return this.deleteOne({Nombre: nombre})
}

module.exports = MateriaModel

module.exports = mongoose.model("Materia", MateriaModel)