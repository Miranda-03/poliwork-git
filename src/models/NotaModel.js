const mongoose = require("mongoose")
const { Schema } = mongoose
const Recordatorio = require("./schemas/RecordatorioShema")
const Reunion = require("./schemas/ReunionShema")
const UsuarioSchema = require('./schemas/UsuarioSchema')

const NotaModel = new Schema({
    Titulo: String,
    Descripcion: String,
    AlumnoCreador: String,
    FechaFinal: Date,
    Estado : String,
    Alumnos: [
        {
            Alumno: String,
            estado: String
        }
    ],
    Recordatorios: [Recordatorio],
    Reuniones: [Reunion]
}, { timestamps: true })

NotaModel.statics.buscarPorId = function (id) {
    return this.findOne({_id: id})
}


NotaModel.statics.deletePorId = function (id) {
    return this.deleteOne({_id: id})
}

NotaModel.statics.cambiarEstado = function (id, estado) {
    return this.findOneAndUpdate({_id : id} , { $set :{Estado : estado}})
}

module.exports = mongoose.model("Nota", NotaModel)
