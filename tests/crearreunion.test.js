const app = require("../app");
const { connectToDB, disconnectToDB } = require('../src/utils/DbUtils')
const request = require("supertest")(app);
const Materia = require('../src/models/MateriaModel')
const mongoose = require("mongoose")

beforeAll(async () => {
  await connectToDB()
  await Materia.deletePorNombre("materia")
})

describe("crearreunion", () => {
  describe("creada correctamente", () => {
    test("retornar 200", async () => {
      let start = Date.now();
      let final = Date.now();
      const materiaTest = {
        Nombre: "materia",
        AlumnoCreador: "alumno",
        Profesor : "Profesor",
        Dias: [{
          Dia: "Viernes",
          Horarios: [{
              inicio: start,
              final: final
          }]
        }]
      };

      const response = await request.post("/materias").send(materiaTest)
      const reunionData = await Materia.findOne({ Nombre : "materia" }, {Nombre : 1}).lean();
      expect(response.statusCode).toBe(200);
      expect(response.body.message).toBe("la materia se agrego con éxito");
      expect(reunionData.Nombre).toBe("materia");
    })
  })
})


afterAll(async () => {
 await mongoose.disconnect()
})
